﻿using Quiz.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Quiz
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void OnStartup(object sender, StartupEventArgs e)
        {
            try
            {
                // Create the ViewModel and expose it using the View's DataContext
                var view = new Views.AuthView();
                AuthViewModel vm = new AuthViewModel();
                vm.ClosingRequest += (sender1, e1) => view.Close();
                view.DataContext = vm;
                view.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
    }
}
