﻿using Quiz.Common;
using System;
using System.Data;
using System.Windows.Input;
using Quiz.Models;
using Quiz.Views;

namespace Quiz.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private DataTable _gridSource;
        private DataRowView _selectedRow;
        public bool IsItemVisible { get; set; }

        public ICommand TakeQuizCommand { get; set; }
        public ICommand AdministrationMenuCommand { get; set; }
        public ICommand LogoutMenuCommand { get; set; }
        public ICommand TestsResultsCommand { get; set; }

        public DataTable GridSource
        {
            get { return _gridSource; }
            set {
                _gridSource = value;
                OnPropertyChanged("GridSource");
            }
        }

        public DataRowView SelectedRow
        {
            get { return _selectedRow; }
            set
            {
                _selectedRow = value;
                OnPropertyChanged("SelectedRow");
            }
        }

        /*
        private DataTable SetGridSource()
        {
            var query = "SELECT * FROM [dbo].[Quiz];";
            var source = DbManager.DbManager.GetTable(query);
            return source;
        }
        */

        private void SetGridSource()
        {
            var query = "SELECT * FROM [dbo].[Quiz];";
            GridSource = DbManager.DbManager.GetTable(query);
            
        }

        private void TakeQuiz()
        {
            if (SelectedRow != null)
            {
                var quizId = (int)SelectedRow["Id"];
                var file = (string)SelectedRow["PathFile"];
                var caption = (string)SelectedRow["Caption"];
                var time = (TimeSpan)SelectedRow["Timer"];
               
                var rep = new QuizMssqlRepository(file, caption, time, quizId);
                var view = new Views.QuizView();

                QuizViewModel vm = new QuizViewModel(rep);
                vm.ClosingRequest += (sender, e) => view.Close();
                view.DataContext = vm;
                view.ShowDialog();
            }
        }

        private void LogoutMenu()
        {
            Global.UserId = 0;
            Global.UserIsAdmin = false;
            Global.UserName = "";
            
            var view = new AuthView();
            AuthViewModel vm = new AuthViewModel();
            vm.ClosingRequest += (sender, e) => view.Close();
            view.DataContext = vm;
            view.Show();
            this.OnClosingRequest();
        }


        private void AdministrationMenu()
        {
            AdminViewModel vm = new AdminViewModel();
            var view = new AdminView();
            vm.ClosingRequest += (sender, e) => view.Close();
            vm.DbRefreshed += (sender, e) => this.SetGridSource();
            view.DataContext = vm;
            view.Show();
        }

        private void TestsResultsMenu()
        {
            var view = new TestResultsView();
            TestsResultsViewModel vm = new TestsResultsViewModel();
            vm.ClosingRequest += (sender, e) => view.Close();
            view.DataContext = vm;
            view.Show();
        }

        public MainViewModel()
        {
            IsItemVisible = Global.UserIsAdmin;
            // GridSource = SetGridSource();
            SetGridSource();
            TakeQuizCommand = new Command(o => TakeQuiz());
            LogoutMenuCommand = new Command(p => LogoutMenu());
            AdministrationMenuCommand = new Command(o => AdministrationMenu());
            TestsResultsCommand = new Command(o => TestsResultsMenu());
        }

    }
}
