﻿using Quiz.Common;
using Quiz.Models;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace Quiz.ViewModels
{
    public class QuizInfo : ViewModelBase
    {
        private List<CheckBox> _answers;
        private string _questionText;

        public List<CheckBox> Answers
        {
            get { return _answers; }
            private set
            {
                _answers = value;
                OnPropertyChanged("Answers");
            }
        }

        public string QuestionText
        {
            get { return _questionText; }
            private set
            {
                _questionText = value;
                OnPropertyChanged("QuestionText");
            }
        }

        public QuizInfo()
        {

        }

        public QuizInfo(string question, List<CheckBox> answers)
        {
            QuestionText = question;
            Answers = answers;
        }

        public QuizInfo(Models.Quiz quiz)
        {
            QuestionText = quiz.Question;
            Answers = quiz.Answers.Select(x => new CheckBox() { Content = x, IsChecked = false }).ToList();
        }
    }
}
