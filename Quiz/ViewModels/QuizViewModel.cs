﻿using Quiz.Common;
using Quiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace Quiz.ViewModels
{
    public sealed class QuizViewModel : ViewModelBase
    {
        
        private IQuizRepository repository; // store of quiz
        private QuizInfo _selectedQuiz; // current quiz info
        private List<QuizInfo> _quiz; // all quiz info
        private Command<string> _selectQuestionCommand; // command for navigation for questions button
        private Command _submitCommand;
        private int currentIndex; //current question number
        DispatcherTimer timer;

        public string QuizCaption { get; set; }
        private string _currentTime;

        private List<Button> _buttons;

        public List<Button> Buttons
        {
            get { return _buttons; }
            set
            {
                _buttons = value;
                OnPropertyChanged("Butons");
            }
        }
      
        public ICommand SelectQuestionCommand
        {
            get
            {
                if (_selectQuestionCommand == null)
                {
                    _selectQuestionCommand = new Command<string>(SelectQuestion);
                }
                return _selectQuestionCommand;
            }
        }

        public ICommand SubmitCommand
        {
            get
            {
                if (_submitCommand == null)
                {
                    _submitCommand = new Command(o => QuizResult());
                }
                return _submitCommand;
            }
        }

        public QuizInfo SelectedQuiz
        {
            get { return _selectedQuiz; }
            private set
            {
                _selectedQuiz = value;
                OnPropertyChanged("SelectedQuiz");
            }
        }

        private void SelectQuestion(string button)
        {
            if (button.Equals("Previous"))
            {
                currentIndex = currentIndex - 1 >= 0 ? currentIndex - 1 : Quiz.Count - 1;
            }
            else if (button.Equals("Next"))
            {
                currentIndex = (currentIndex + 1) % Quiz.Count;
            }
            else
            {
                currentIndex = Convert.ToInt32(button) - 1;
            }
            SelectedQuiz = Quiz[currentIndex];
        }

        private void QuizResult()
        {
            var checkedAnswers = new int[Quiz.Count][];
            // getting arrays of checked by user aswers
            for (int i = 0; i < Quiz.Count; i++)
            {
                checkedAnswers[i] = Quiz[i].Answers.Select((item, index) => new { item, index }).Where(y => y.item.IsChecked == true).Select(z => z.index).ToArray();
            }
            var res = repository.Quizes.Select(x => x.CorrectAnswers).ToArray(); // getting array of correct answers
            double grade = 0;

            //compare of arrays
            for (int i = 0; i < checkedAnswers.Length; i++)
            {
                if (checkedAnswers[i].SequenceEqual(res[i])) grade++;
            }
            var result = String.Format("{0:0.00}", grade / repository.Quizes.Count());
            timer.Stop();
            this.OnClosingRequest();
            MessageBox.Show("Your result is " + grade + " from " + repository.Quizes.Count().ToString() + "( " + result + "%)", "Result");

            var user = Global.UserId.ToString();
            var testId = repository.QuizId.ToString();
           

            var query = "INSERT INTO [dbo].[Result] VALUES('" + user + "','" + result + "','" + testId + "','" + DateTime.Now.ToString() + "');";
         //   MessageBox.Show(query);
            DbManager.DbManager.ExecuteCommand(query);
            
        }

        public List<QuizInfo> Quiz
        {
            get { return _quiz; }
            set
            {
                _quiz = value;
                OnPropertyChanged("Quiz");
            }
        }

        public string CurrentTime
        {
            get
            {
                return this._currentTime;
            }
            set
            {
                if (_currentTime == value)
                    return;
                _currentTime = value;
                OnPropertyChanged("CurrentTime");
            }
        }

        private void TimerStart()
        {
            TimeSpan time = TimeSpan.Parse(CurrentTime);
            timer = new DispatcherTimer(DispatcherPriority.Render, Application.Current.Dispatcher);
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += (s, e) => {
                CurrentTime = time.ToString("c");
                if (time == TimeSpan.Zero)
                {
                    timer.Stop();
                    QuizResult();
                }
                time = time.Add(TimeSpan.FromSeconds(-1));
            };
            timer.Start();
        }

        public QuizViewModel(IQuizRepository repository)
        {
            this.repository = repository;
            QuizCaption = "Quiz - " + repository.Caption;
            CurrentTime = repository.Timer.ToString();
            Quiz = repository.Quizes.Select(x => new QuizInfo(x)).ToList();
            SelectedQuiz = Quiz[0];
            currentIndex = 0;
            Buttons = Quiz.Select((x, index) => new Button { Content = (index + 1).ToString() }).ToList();
            TimerStart();
        }
    }
}
