﻿using Quiz.Common;
using Quiz.Models;
using System.Data;

namespace Quiz.ViewModels
{
    public class TestsResultsViewModel : ViewModelBase
    {
        private DataTable _gridSource;

        public DataTable GridSource
        {
            get { return _gridSource; }
            set
            {
                _gridSource = value;
                OnPropertyChanged("GridSource");
            }
        }

        private DataTable GetGrid()
        {
            int id = Global.UserId;
            var query = "";
            if (Global.UserIsAdmin)
            {
                query = "SELECT [dbo].[User].[Login], [dbo].[Quiz].[Caption], [dbo].[Result].[Result], [dbo].[Result].[Date] FROM [dbo].[User], [dbo].[Quiz], [dbo].[Result] WHERE [dbo].[User].[Id] = [dbo].[Result].[UserId] AND [dbo].[Result].[TestId] = [dbo].[Quiz].[Id];";
            }
            else
            {
                query = "SELECT [dbo].[Quiz].[Caption], [dbo].[Result].[Result], [dbo].[Result].[Date] FROM [dbo].[Quiz], [dbo].[Result] WHERE UserId='" + id.ToString() + "' AND [dbo].[Result].[TestId]=[dbo].[Quiz].[Id];";
            }
            return DbManager.DbManager.GetTable(query);
        }

        public TestsResultsViewModel()
        {
            GridSource = GetGrid();
        }
    }
}
