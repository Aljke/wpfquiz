﻿using Microsoft.Win32;
using Quiz.Common;
using Quiz.Views;
using System;
using System.Data;
using System.IO;
using System.Windows.Input;

namespace Quiz.ViewModels
{
    public class AdminViewModel : ViewModelBase
    {
        private DataTable _gridSource;
        private DataRowView _selectedRow;

        public bool IsReadOnly { get; set; }
        public string EditFormTitle { get; set; }
        public string ActionTitle { get; set; }

        public event EventHandler DbRefreshed;

        protected void OnDbRefreshed()
        {
            if (this.DbRefreshed != null)
            {
                this.DbRefreshed(this, EventArgs.Empty);
            }
        }

        public ICommand ActionCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ChooseCommand { get; set; }

        public DataTable GridSource
        {
            get { return _gridSource; }
            set
            {
                _gridSource = value;
                OnPropertyChanged("GridSource");
            }
        }

        public DataRowView SelectedRow
        {
            get { return _selectedRow; }
            set
            {
                _selectedRow = value;
                OnPropertyChanged("SelectedRow");
            }
        }

        private DataTable SetGridSource()
        {
            var query = "SELECT * FROM [dbo].[Quiz];";
            var source = DbManager.DbManager.GetTable(query);
            return source;
        }

        private void ShowEditView(EditMode editMode)
        {
            var view = new AdminViewDialog();
            CancelCommand = new Command(o => view.Close());
            view.DataContext = this;

            if (editMode == EditMode.Add) SelectedRow = GridSource.AsDataView().AddNew();

            switch (editMode)
            {
                case EditMode.Add:
                    {
                        EditFormTitle = "Add new test";
                        ActionTitle = "Save";
                        IsReadOnly = false;
                        
                        ActionCommand = new Command(o =>
                        {
                            var id = SelectedRow["Id"].ToString();
                            var file = (string)SelectedRow["PathFile"];
                            var caption = (string)SelectedRow["Caption"];
                            var timer = SelectedRow["Timer"].ToString();
                            var questions = (string)SelectedRow["Questions"];
                            var query = "INSERT INTO [dbo].[Quiz] VALUES('" + file + "','" + caption + "','"
                            + timer + "','" + questions + "');";
                            CrudAction(query);
                            view.Close();
                        });
                        break;
                    }
                case EditMode.Edit:
                    {
                        EditFormTitle = "Editing";
                        ActionTitle = "Save";
                        IsReadOnly = false;
                        ActionCommand = new Command(o =>
                        {
                            var id = SelectedRow["Id"].ToString();
                            var file = (string)SelectedRow["PathFile"];
                            var caption = (string)SelectedRow["Caption"];
                            var timer = SelectedRow["Timer"].ToString();
                            var questions = (string)SelectedRow["Questions"];
                            var query = "UPDATE [dbo].[Quiz] SET PathFile='" + file + "', Caption='" + caption + "', Timer='" + timer
                            + "', Questions='" + questions + "' WHERE Id='" + id +"';";
                            CrudAction(query);
                            view.Close();
                        });
                        break;
                    }
                case EditMode.Delete:
                    {
                        EditFormTitle = "Remove test";
                        ActionTitle = "Delete";
                        IsReadOnly = true;
                        ActionCommand = new Command(o =>
                        {
                            var id = SelectedRow["Id"].ToString();
                            var file = (string)SelectedRow["PathFile"];
                            var caption = (string)SelectedRow["Caption"];
                            var timer = SelectedRow["Timer"].ToString();
                            var questions = (string)SelectedRow["Questions"];
                            var query = "DELETE FROM [dbo].[Quiz] WHERE Id='" + id + "';";
                            CrudAction(query);
                            view.Close();
                        });
                        break;
                    }
                default:
                    break;
            }
            view.ShowDialog();
        }

        private void CrudAction(string query)
        {
            DbManager.DbManager.ExecuteCommand(query);
            GridSource = SetGridSource();
            OnDbRefreshed();
        }

        private void Choose()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "JSON Files(*.json) |*.json| All files(*.*) |*.*";
            openFileDialog.Multiselect = false;
            if (openFileDialog.ShowDialog() == true)
            {
                SelectedRow["PathFile"] = @"JSON\" + openFileDialog.SafeFileName;
            }
        }

        public AdminViewModel()
        {
            GridSource = SetGridSource();
            if (GridSource.Rows.Count > 0) SelectedRow = GridSource.AsDataView()[0];
            AddCommand = new Command((o) => ShowEditView(EditMode.Add));
            EditCommand = new Command((o) => ShowEditView(EditMode.Edit));
            DeleteCommand = new Command((o) => ShowEditView(EditMode.Delete));
            ChooseCommand = new Command(o => Choose());
        }
    }
}
