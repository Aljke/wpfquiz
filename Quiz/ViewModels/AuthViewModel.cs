﻿using Quiz.Common;
using Quiz.Models;
using System.Data;
using System.Windows.Input;

namespace Quiz.ViewModels
{
    public class AuthViewModel : ViewModelBase
    {
        private string _login;
        private string _password;
        private bool _isLabelVisible;

        public ICommand LoginCommand { get; set; }

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                OnPropertyChanged("Login");
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged("Password");
            }
        }

        public bool IsLabelVisible
        {
            get { return _isLabelVisible; }
            set
            {
                _isLabelVisible = value;
                OnPropertyChanged("IsLabelVisible");
            }
        }

        private void LogIn()
        {
            string query = "SELECT * FROM [dbo].[User] WHERE Login='" + Login + "' AND Password='" + Password + "';";
            var queryRes = DbManager.DbManager.GetTable(query);
            if (queryRes.Rows.Count > 0)
            {
                Global.UserName = queryRes.Rows[0].Field<string>("Login");
                Global.UserIsAdmin = queryRes.Rows[0].Field<bool>("IsAdmin");
                Global.UserId = queryRes.Rows[0].Field<int>("Id");

                var view = new Views.MainView();
                MainViewModel vm = new MainViewModel();
                vm.ClosingRequest += (sender, e) => view.Close();
                view.DataContext = vm;
                view.Show();
                this.OnClosingRequest();
            }
            else
            {
                IsLabelVisible = true;
            }
        }

        public AuthViewModel()
        {
            LoginCommand = new Command(o => LogIn());
        }
    }
}
