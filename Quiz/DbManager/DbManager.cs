﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz.DbManager
{
    public static class DbManager
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["QUIZDB"].ConnectionString;

        public static void ExecuteCommand(string query)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            con.Open();
            SqlCommand command = new SqlCommand(query, con);
            command.ExecuteNonQuery();
            con.Close();
        }

        public static DataTable GetTable(string query)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlDataAdapter dbAdapter = new SqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                dbAdapter.Fill(dt);
                return dt;
            }
        }
    }
}
