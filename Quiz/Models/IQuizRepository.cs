﻿using System;
using System.Collections.Generic;

namespace Quiz.Models
{
    public interface IQuizRepository
    {
        string Caption { get; }
        TimeSpan Timer { get; }
        int QuizId { get; set; }
        IEnumerable<Quiz> Quizes { get; }
        Quiz this[int i] { get; }
    }
}
