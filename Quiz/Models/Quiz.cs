﻿using Newtonsoft.Json;

namespace Quiz.Models
{
    public class Quiz
    {
        [JsonProperty("question")]
        public string Question;

        [JsonProperty("answers")]
        public string[] Answers;

        [JsonProperty("correctAnswers")]
        public int[] CorrectAnswers;

        public Quiz(string question, string[] answers, int[] correct)
        {
            Question = question;
            Answers = answers;
            CorrectAnswers = correct;
        }

        public Quiz()
        {

        }
    }
}
