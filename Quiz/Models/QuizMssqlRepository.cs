﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Quiz.Models
{
    public class QuizMssqlRepository : IQuizRepository
    {
        public string Caption { get; }
        public TimeSpan Timer { get; }
        public IEnumerable<Quiz> Quizes { get; }
        public int QuizId { get; set; }

        public Quiz this[int i] => Quizes.ElementAt(i);

        private IEnumerable<Quiz> GetQuizData(string filePath)
        {
            using (StreamReader r = new StreamReader(filePath))
            {
                string json = r.ReadToEnd();
                IEnumerable<Quiz> items = JsonConvert.DeserializeObject<IEnumerable<Quiz>>(json);
                return items;
            }
        }

        public QuizMssqlRepository(string filePath, string caption, TimeSpan timer, int id)
        {
            QuizId = id;
            Caption = caption;
            Timer = timer;
            Quizes = GetQuizData(filePath);
        }
    }
}
